#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/kprobes.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/slab.h>
#include <asm/uaccess.h>

#define SIZE 10
static int pid;
unsigned long temp;
long int time;

struct queue {
	unsigned long **data;
	struct timespec time[10];
	int front;
	int rear;
}que;

static long j_handle_mm_fault(struct mm_struct *mm, struct vm_area_struct *vma, unsigned long address,
 unsigned int flags) {
	/*if(current->pid == pid) {
		printk(KERN_INFO "\nIn handle of jprobe addr is: %lx pid: %d", address, current->pid);
	}*/
	if (que.rear >= SIZE)
		que.rear = 0;
	*que.data[que.rear] = address;
	getnstimeofday(&que.time[que.rear++]);
	jprobe_return();
	return 0;
}

static struct jprobe my_jprobe = {
	.entry = j_handle_mm_fault,
	.kp = {
		.symbol_name = "handle_mm_fault",
	},
};

int device_open(struct inode *inode, struct file *filp) {
	printk(KERN_INFO "\nOpenened device");
	return 0;
}

ssize_t device_read(struct file* flip, char* bufStoreData, size_t bufCount, loff_t *curOffset) {
	snprintf(bufStoreData, bufCount, "%lx", *que.data[que.front]);
	time = que.time[que.front++].tv_nsec;
	printk(KERN_INFO "\nCopied to USER: %lx and Time is: %lu", *que.data[que.front++],time);
	if(que.front >= SIZE)
		que.front = 0;
	return time;
}

ssize_t device_write(struct file* flip, char* bufSourceData, size_t bufCount, loff_t *curOffset) {
	return 0;
}

int device_close(struct inode *inode, struct file *flip) {
	printk(KERN_INFO "\nClosing device");
	return 0;
}

static struct file_operations fops = {
	.owner = THIS_MODULE,
	.open = device_open,
	.release = device_close,
	.write = device_write,
	.read = device_read
};

struct cdev *mcdev;
int major_number;
dev_t dev_num;

#define DEVICE_NAME	"probes"

static int __init jprobe_init(void){
	int ret,i;
	//Inserting / creating Module
	ret = alloc_chrdev_region(&dev_num,0,1,DEVICE_NAME);
	if(ret < 0) {
		printk(KERN_ALERT "FAILED TO ALLOCATE MAJOR NUMBER");
		return ret;
	}
	major_number= MAJOR(dev_num);
	printk(KERN_INFO "ProducerConsumer: major number is:%d",major_number);
	printk(KERN_INFO "\tuse \"mknod /dev/%s c %d 0\" for device file", DEVICE_NAME, major_number);

	mcdev = cdev_alloc();
	mcdev->ops = &fops;
	mcdev->owner = THIS_MODULE;

	ret = cdev_add(mcdev, dev_num, 1);
	if(ret < 0) {
		printk(KERN_ALERT "FAILED TO ADD TO KERNEL");
		return ret;
	}
	//Allocate buffer
	que.data = (unsigned long **)kmalloc(sizeof(unsigned long *) * SIZE, GFP_KERNEL);
	for(i=0;i<SIZE;i++)
		que.data[i] = (unsigned long *)kmalloc(sizeof(unsigned long), GFP_KERNEL);
	//Registering Module
	ret = register_jprobe(&my_jprobe);
	if (ret < 0) {
		 printk(KERN_INFO "\nFailed to register jprobe");
		 return -1;
	}
	printk(KERN_INFO "planted jprobe at :%p, handler addr: %p\n with pid as: %d", my_jprobe.kp.addr, my_jprobe.entry, pid);
	return 0;
}
static int __exit jprobe_exit(void){
	 cdev_del(mcdev);
	 unregister_chrdev_region(dev_num, 1);
	 unregister_jprobe(&my_jprobe);
	 printk(KERN_INFO "\nUnregister the jprobe at : %p\n",my_jprobe.kp.addr);
	 return 0;
}

module_param(pid, int, S_IRUGO);
module_init(jprobe_init)
module_exit(jprobe_exit)
MODULE_LICENSE("GPL"); 