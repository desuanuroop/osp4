#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/wait.h>
#include <signal.h>
#include <string.h>
#include <pthread.h>
#include <semaphore.h>
#include <time.h>
#define SIZE 10
#define NUM_COMMANDS 2

void plot(unsigned long *, long int time[SIZE]);

void main() {
	int fd,i;
	long int time[SIZE];
	char **data, *ptr;
	unsigned long *ldata;
	fd = open("/dev/probes", O_RDONLY);
	if (fd == -1)
		printf("\nModules can't be opened\n");
	else 
		printf("\nModule is opened\n");
	data = (char **)malloc(sizeof(char *) * 10);
	ldata = (unsigned long *)malloc(sizeof(unsigned long *) * 10);
	for(i=0;i<10;i++){
		data[i] = (char *)malloc(sizeof(char) * 13);
		time[i] = read(fd, data[i], 13);
		ldata[i] = strtoul(data[i], &ptr, 16);		
		printf("%d. Retrieved Data is: %lx, Time is: %lu\n",i+1,ldata[i], time[i]);
	}
	//Following is to convert back char VA to unsigned Long 
/*	char *ptr;
	unsigned long rets;
	rets = strtoul(data[0], &ptr, 16);
	printf("RETS IS: %lx\n", rets);
	//Following is to convert back char VA to long double 
	double temp;
	temp = atof(data[0]);
	sscanf(data[0], "%lf", &temp);
	printf("Double value is: %lf\n", temp);*/
	plot(ldata, time);
}

void plot(unsigned long *ldata, long int time[SIZE])
{
    char * commandsForGnuplot[] = {"set title \"TITLEEEEE\"", "plot 'data.temp'"};
    unsigned long xvals[SIZE] = {1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0,10.0};
    //unsigned long yvals[NUM_POINTS] = {500,3, 1, 3, 50};
    FILE * temp = fopen("data.temp", "w");
    /*Opens an interface that one can use to send commands as if they were typing into the
     *     gnuplot command line.  "The -persistent" keeps the plot open even after your
     *     C program terminates.
     */
    FILE * gnuplotPipe = popen ("gnuplot -persistent", "w");
    int i;
    for (i=0; i < SIZE; i++)
    {
 	   fprintf(temp, "%llu %lx \n", xvals[i], ldata[i]); //Write the data to a temporary file
    }

    for (i=0; i < NUM_COMMANDS; i++)
    {
    fprintf(gnuplotPipe, "%s \n", commandsForGnuplot[i]); //Send commands to gnuplot one by one.
    }
	fflush(gnuplotPipe);
}