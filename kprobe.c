#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/kprobes.h>

static struct kprobe kp = {
	.symbol_name = "_do_fork",
};

static int handler_pre(struct kprobe *p, struct pt_regs *regs) {
	printk(KERN_INFO "pre_handler ip: ");
	return 0;
}

static int handler_post(struct kprobe *p, struct pt_regs *regs,unsigned long flags){
	printk(KERN_INFO "post_handler ip: ");
	return 0;
}

static int handler_fault(struct kprobe *p, struct pt_regs *regs, int trapnr) {
	printk(KERN_INFO "trap no: ");
	return 0;
}

static int __init kprobe_init(void) {
	int ret;
	kp.pre_handler = handler_pre;
	kp.post_handler = handler_post;
	kp.fault_handler = handler_fault;

	ret = register_kprobe(&kp);
	if (ret < 0){
		printk(KERN_INFO "registering kprobe failed");
		return ret;
	}
	printk(KERN_INFO "planted probe at addr: %p", kp.addr);
	return 0;
}

static void __exit kprobe_exit(void){
	unregister_kprobe(&kp);
	printk(KERN_INFO "Kprobe unregistered");
}

module_init(kprobe_init)
module_exit(kprobe_exit)
MODULE_LICENSE("GPL");
